provider "aws" {
  assume_role {
    role_arn = "${var.workspace_iam_roles[terraform.workspace]}"
  }
}

terraform {
  backend "s3" {
    bucket  = "gloom-terraform"
    key     = "aws/2048/gloom.tfstate"
    encrypt = true
    region  = "us-west-2"
  }
}

data "terraform_remote_state" "vpc" {
  backend = "s3"

  config {
    bucket = "gloom-terraform"
    key    = "env:/${terraform.workspace}/aws/gloom.tfstate"
    region = "us-west-2"
  }
}

module "2048_demo" {
  source          = "git::https://gitlab.com/terraform_modules/2048.git"
  region          = "us-west-2"
  logging_enabled = "false"
  domain          = "coyne.link"
  vpc_s3_key      = "aws/gloom.tfstate"
  security_groups = "${data.terraform_remote_state.vpc.2048_sg}"
  public_subnets  = "${data.terraform_remote_state.vpc.public_subnets}"
  vpc_id          = "${data.terraform_remote_state.vpc.vpc_id}"
  ssh_key         = "${data.terraform_remote_state.vpc.personal_key0}"
  logging_bucket  = "gloom-logging"
}
